package hw;

import java.util.Calendar;

public class HelloWorld {

  public String getMessage() {
    return "hello world";
  }

  public int getYear() {
    return Calendar.getInstance().get(Calendar.YEAR);

  }
}
